package Unitests

import (
	"encoding/json"
	"fmt"
	"golang_rest_api/Src/DbContext"
	"golang_rest_api/Src/Models"
	"golang_rest_api/Src/Repositories"
	"golang_rest_api/Src/Services"
	"testing"
)

func TestRegister(t *testing.T) {
	postGresSqlDbConnection := DbContext.GetConnection()
	var userRepository = Repositories.NewUserRepository(postGresSqlDbConnection)
	var userService = Services.NewUserService(userRepository)

	params := Models.RegisterRequestModel{
		UserName:        "dika6690",
		Password:        "12345678",
		ConfirmPassword: "12345678",
		Email:           "dika@gmail.com",
	}
	js, _ := json.Marshal(params)
	sj := string(js)
	fmt.Print(sj)
	userService.Register(params)
}

func TestLogin(t *testing.T) {
	postGresSqlDbConnection := DbContext.GetConnection()
	var userRepository = Repositories.NewUserRepository(postGresSqlDbConnection)
	var userService = Services.NewUserService(userRepository)

	params := Models.LoginRequestModel{
		Password: "12345678",
		Email:    "dika@gmail.com",
	}
	js, _ := json.Marshal(params)
	sj := string(js)
	fmt.Print(sj)
	userService.Login(params)
}
