package Unitests

import (
	"fmt"
	"math"
	"testing"
)

func TestReverse(t *testing.T) {
	res := reverse([]int32{1, 5, 3, 4, 7, 8})

	fmt.Print(res)
}

func reverse(a []int32) []int32 {

	l := len(a)
	var it int64 = int64(math.Floor(float64(l) / 2))
	for i := 0; int64(i) < it; i++ {
		a[i], a[(l-1)-i] = a[(l-1)-i], a[i]
	}

	return a
}

func Test2DArray(t *testing.T) {
	arr := [][]int32{
		{1, 1, 1, 0, 0, 0},
		{0, 1, 0, 0, 0, 0},
		{1, 1, 1, 0, 0, 0},
		{0, 0, 2, 4, 4, 0},
		{0, 0, 0, 2, 0, 0},
		{0, 0, 1, 2, 4, 0},
	}

	res := Array2D(arr)

	fmt.Print(res)
}

func Array2D(arr [][]int32) int32 {

	var tempArr [16][]int32

	for i := 0; i < 16; i++ {
		nl := int(math.Floor(float64(i) / 4))
		ia := i - (nl * 4)
		a := arr[nl][ia:(ia + 3)]
		b := arr[(nl + 1)][(ia + 1) : ia+2]
		c := arr[nl+2][ia:(ia + 3)]

		tempArr[i] = append(tempArr[i], a...)
		tempArr[i] = append(tempArr[i], b...)
		tempArr[i] = append(tempArr[i], c...)
	}

	resArr := []int32{}

	max := int32(math.MinInt32)
	for _, ea := range tempArr {
		sum := int32(0)
		for _, eb := range ea {

			sum += eb

		}
		if sum > max {
			max = sum
		}
		resArr = append(resArr, sum)
	}

	return max
}
