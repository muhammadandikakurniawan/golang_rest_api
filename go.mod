module golang_rest_api

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.9.0
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	gorm.io/driver/postgres v1.0.6
	gorm.io/gorm v1.20.11
)
