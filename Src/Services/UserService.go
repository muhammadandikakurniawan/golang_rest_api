package Services

import (
	"crypto/sha1"
	"fmt"
	"golang_rest_api/Src/Entities"
	"golang_rest_api/Src/Helpers"
	"golang_rest_api/Src/Models"
	"golang_rest_api/Src/Repositories"

	"time"

	uuid "github.com/nu7hatch/gouuid"
	"golang.org/x/crypto/bcrypt"
)

func NewUserService(userRepository Repositories.IUserRepository) IUserService {
	return &UserService{
		userRepository: userRepository,
	}
}

type IUserService interface {
	Register(Models.RegisterRequestModel) Models.ResultValueModel
	GetToken(Models.LoginRequestModel) Models.ResultValueModel
	Login(Models.LoginRequestModel) Models.ResultValueModel
	GetUserById(id string) (Entities.UserEntity, bool)
}

type UserService struct {
	userRepository Repositories.IUserRepository
}

func (this *UserService) Register(p Models.RegisterRequestModel) Models.ResultValueModel {

	result := Models.ResultValueModel{
		StatusCode: "00",
		Message:    "success access register service",
	}

	if p.ConfirmPassword != p.Password {
		result.StatusCode = "10"
		result.Message = "password not matching"
		return result
	}

	if p.Password == "" {
		result.StatusCode = "10"
		result.Message = "password not matching"
		return result
	}

	if p.Email == "" {
		result.StatusCode = "10"
		result.Message = "Email can't be empty"
		return result
	}

	if p.UserName == "" {
		result.StatusCode = "10"
		result.Message = "UserName can't be empty"
		return result
	}

	uuidRes, uuidErr := uuid.NewV4()

	if uuidErr != nil {
		result.StatusCode = "500"
		result.Message = "error generate uuid"
		return result
	}

	passwordByte := []byte(p.Password)
	passwordHash, hashErr := bcrypt.GenerateFromPassword(passwordByte, bcrypt.DefaultCost)
	if hashErr != nil {
		result.StatusCode = "500"
		result.Message = "hasing password failed"
		return result
	}
	p.Password = string(passwordHash)

	dataInsert := Entities.UserEntity{
		Id:       uuidRes.String(),
		Username: p.UserName,
		Email:    p.Email,
		Password: p.Password,
	}
	insertRes := this.userRepository.Create(dataInsert)
	result.StatusCode = "00"
	result.Message = "Success"
	if !insertRes.IsSuccess {
		result.StatusCode = "500"
		result.Message = fmt.Sprintf("Insert failed : %s", insertRes.Message)
	}

	result.Value = Models.UserModel{
		Id:       dataInsert.Id,
		UserName: p.UserName,
		Email:    p.Email,
		Password: p.Password,
	}

	return result
}

func (this *UserService) GetToken(p Models.LoginRequestModel) Models.ResultValueModel {
	result := Models.ResultValueModel{
		StatusCode: "00",
		Message:    "success access GetToken service",
	}

	uuidRes, uuidErr := uuid.NewV4()

	if uuidErr != nil {
		result.StatusCode = "500"
		result.Message = "error generate uuid"
		return result
	}

	dataUser := Models.UserModel{
		Id:       uuidRes.String(),
		Email:    p.Email,
		Password: p.Password,
	}

	hasher := sha1.New()

	hasher.Write([]byte(dataUser.Id))

	token := string(hasher.Sum(nil))

	result.Value = Models.TokenResultModel{
		User:  dataUser,
		Token: token,
	}

	return result
}

func (this *UserService) GetUserById(id string) (Entities.UserEntity, bool) {
	return this.userRepository.GetById(id)
}

func (this *UserService) Login(p Models.LoginRequestModel) Models.ResultValueModel {
	result := Models.ResultValueModel{
		StatusCode: "00",
		Message:    "success access Login service",
	}

	dataUser, dataUserNull := this.userRepository.GetByEmail(p.Email)

	if dataUserNull {
		result.StatusCode = "40"
		result.Message = "User not found"
		return result
	}

	errComparePass := bcrypt.CompareHashAndPassword([]byte(dataUser.Password), []byte(p.Password))
	if errComparePass != nil {
		result.StatusCode = "41"
		result.Message = "Password wrong"
		return result
	}

	dataClaims := Models.UserModel{
		Email:    dataUser.Email,
		Id:       dataUser.Id,
		UserName: dataUser.Username,
	}
	exp := time.Now().Add(time.Minute * 15)
	tokenStr, tokenError := Helpers.CreateJwtToken(dataClaims, Helpers.JWT_SIGNATURE_KEY, exp.Unix())
	if tokenError != nil {
		result.StatusCode = "42"
		result.Message = "Create Token failed"
		return result
	}
	result.Value = Models.LoginResulttModel{
		Token:           tokenStr,
		TokenExpiryDate: exp.Format("Jan 2, 2006 at 3:04pm (MST)"),
	}

	return result
}
