package Services

import (
	"fmt"
	"golang_rest_api/Src/Entities"
	"golang_rest_api/Src/Models"
	"golang_rest_api/Src/Repositories"
)

func NewProductService(productRepository Repositories.IProductRepository) IProductService {
	service := &ProductService{
		productRepository: productRepository,
	}

	return service
}

type IProductService interface {
	GetProduct() Models.DataPageResultModel
	CreateProduct(param Entities.ProductEntity) Models.ResultValueModel
	UpdateProduct(param Entities.ProductEntity) Models.ResultValueModel
	DeleteProduct(id int) Models.ResultValueModel
}

type ProductService struct {
	productRepository Repositories.IProductRepository
}

func (this *ProductService) GetProduct() Models.DataPageResultModel {
	result := Models.DataPageResultModel{
		StatusCode: "00",
	}

	var rows = this.productRepository.GetAll()

	result.TotalData = len(rows)
	result.Value = rows

	return result
}

func (this *ProductService) CreateProduct(param Entities.ProductEntity) Models.ResultValueModel {

	result := Models.ResultValueModel{
		StatusCode: "00",
	}

	insertRes := this.productRepository.Create(param)
	result.Value = param
	if !insertRes.IsSuccess {
		result.StatusCode = "500"
		result.Message = "insert datafailed : " + insertRes.Message
	}

	return result
}

func (this *ProductService) UpdateProduct(param Entities.ProductEntity) Models.ResultValueModel {
	result := Models.ResultValueModel{
		StatusCode: "00",
	}

	if param.Id <= 0 {
		result.StatusCode = "100"
		result.Message = "id cannot be null"
	} else {
		_, getByIdIsNull := this.productRepository.GetById(param.Id)

		if getByIdIsNull {
			result.StatusCode = "101"
			result.Message = "data with id :" + fmt.Sprint(param.Id) + " is not found"
		} else {
			updateRes := this.productRepository.Update(param)

			if !updateRes.IsSuccess {
				result.StatusCode = "102"
				result.Message = "update failed :" + updateRes.Message
			} else {
				result.Value = param
			}
		}

	}

	return result
}

func (this *ProductService) DeleteProduct(Id int) Models.ResultValueModel {
	result := Models.ResultValueModel{
		StatusCode: "00",
	}

	if Id <= 0 {
		result.StatusCode = "100"
		result.Message = "id cannot be null"
	} else {
		dataOld, getByIdIsNull := this.productRepository.GetById(Id)

		if getByIdIsNull {
			result.StatusCode = "101"
			result.Message = "data with id :" + fmt.Sprint(Id) + " is not found"
		} else {
			deleteRes := this.productRepository.Delete(Id)
			if !deleteRes.IsSuccess {
				result.StatusCode = "102"
				result.Message = "update failed :" + deleteRes.Message
			} else {
				result.Value = dataOld
			}
		}

	}

	return result
}
