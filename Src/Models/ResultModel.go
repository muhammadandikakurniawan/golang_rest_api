package Models

type ResultValueModel struct {
	StatusCode string      `json:"StatusCode"`
	Message    string      `json:"Message"`
	Value      interface{} `json:"Value"`
}

type RepoResultModel struct {
	IsSuccess bool        `json:"IsSuccess"`
	Message   string      `json:"Message"`
	Value     interface{} `json:"Value"`
}

type DataPageResultModel struct {
	StatusCode  string      `json:"StatusCode"`
	Message     string      `json:"Message"`
	CurrentPage int         `json:"CurrentPage"`
	TotalPage   int         `json:"TotalPage"`
	TotalData   int         `json:"TotalData"`
	Value       interface{} `json:"Value"`
}
