package Models

type UserModel struct {
	Id       string `json:"Id"`
	UserName string `json:"UserName"`
	Email    string `json:"Email"`
	Password string `json:"Password"`
}

type RegisterRequestModel struct {
	UserName        string `json:"UserName"`
	Email           string `json:"Email"`
	Password        string `json:"Password"`
	ConfirmPassword string `json:"ConfirmPassword"`
}

type LoginRequestModel struct {
	Email    string `json:"Email"`
	Password string `json:"Password"`
}

type LoginResulttModel struct {
	Token           string
	TokenExpiryDate string
}

type TokenResultModel struct {
	User  UserModel
	Token string
}
