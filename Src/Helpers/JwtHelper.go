package Helpers

import (
	"fmt"
	"golang_rest_api/Src/Models"

	"github.com/dgrijalva/jwt-go"
)

var JWT_SIGNING_METHOD = jwt.SigningMethodHS256
var JWT_SIGNATURE_KEY = "TheSecretKeyOfThisProject"

func CreateJwtToken(param Models.UserModel, secret string, exp int64) (string, error) {

	token := jwt.NewWithClaims(JWT_SIGNING_METHOD, jwt.MapClaims{
		"user_ame":   param.UserName,
		"email":      param.Email,
		"user_id":    param.Id,
		"exp":        exp,
		"authorized": true,
	})

	tokenString, err := token.SignedString([]byte(secret))

	if err != nil {
		fmt.Print(err)
	}

	return tokenString, err
}

func VerifyToken(tokenStr string) (jwt.MapClaims, error) {
	tokenRes, tokenErr := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Token is not valid")
		} else if method != JWT_SIGNING_METHOD {
			return nil, fmt.Errorf("Token is not valid")
		}

		return []byte(JWT_SIGNATURE_KEY), nil
	})

	if tokenErr != nil {
		return nil, tokenErr
	}

	claims, ok := tokenRes.Claims.(jwt.MapClaims)

	if !ok || !tokenRes.Valid {
		return nil, fmt.Errorf("Token is not valid")
	}

	return claims, nil
}
