package Middlewares

import (
	"context"
	"fmt"
	"golang_rest_api/Src/DbContext"
	"golang_rest_api/Src/Helpers"
	"golang_rest_api/Src/Repositories"
	"golang_rest_api/Src/Services"
	"net/http"
	"strings"
)

//JwtAuthMiddleware jwt auth
func JwtAuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Print("================== jwt uth middleware ================")

			headerToken := r.Header.Get("Authorization")
			tokenChunk := strings.Split(headerToken, " ")

			if len(tokenChunk) != 2 || tokenChunk[0] == "" {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			tokenStr := tokenChunk[1]

			claims, err := Helpers.VerifyToken(tokenStr)

			if err != nil {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			userId := claims["user_id"]
			if userId == nil {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			userService := Services.NewUserService(Repositories.NewUserRepository(DbContext.GetConnection()))

			dataUser, isnil := userService.GetUserById(fmt.Sprintf("%v", userId))

			if isnil {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			fmt.Print(dataUser)

			ctx := context.WithValue(context.Background(), "userInfo", claims)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)

		})
}
