package Controllers

import (
	"encoding/json"
	"golang_rest_api/Src/Entities"
	"golang_rest_api/Src/Middlewares"
	"golang_rest_api/Src/Services"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func NewProductController(router *mux.Router, productService Services.IProductService) *ProductController {
	controller := &ProductController{
		productService: productService,
	}

	subRoute := router.PathPrefix("/api/Product").Subrouter()
	subRoute.Use(Middlewares.JwtAuthMiddleware)
	subRoute.HandleFunc("/GetProduct", controller.GetProduct).Methods("GET")
	subRoute.HandleFunc("/GetProduct", controller.GetProduct).Methods("GET")
	subRoute.HandleFunc("/CreateProduct", controller.CreateProduct).Methods("POST")
	subRoute.HandleFunc("/UpdateProduct", controller.UpdateProduct).Methods("PUT")
	subRoute.HandleFunc("/DeleteProduct/{id}", controller.DeleteProduct).Methods("DELETE")

	return controller
}

type IProductController interface {
	GetProduct(w http.ResponseWriter, r *http.Request)
	CreateProduct(w http.ResponseWriter, r *http.Request)
	UpdateProduct(w http.ResponseWriter, r *http.Request)
	DeleteProduct(w http.ResponseWriter, r *http.Request)
}

type ProductController struct {
	productService Services.IProductService
}

func (this *ProductController) GetProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	results := this.productService.GetProduct()

	json.NewEncoder(w).Encode(results)
}

func (this *ProductController) CreateProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var body Entities.ProductEntity
	json.NewDecoder(r.Body).Decode(&body)

	result := this.productService.CreateProduct(body)

	json.NewEncoder(w).Encode(result)
}

func (this *ProductController) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var body Entities.ProductEntity
	json.NewDecoder(r.Body).Decode(&body)

	result := this.productService.UpdateProduct(body)

	json.NewEncoder(w).Encode(result)
}

func (this *ProductController) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	idParam := mux.Vars(r)["id"]
	id, idErr := strconv.Atoi(idParam)

	if idErr != nil {
		id = 0
	}

	result := this.productService.DeleteProduct(id)

	json.NewEncoder(w).Encode(result)
}
