package Controllers

import (
	"encoding/json"
	"golang_rest_api/Src/Models"
	"golang_rest_api/Src/Services"
	"net/http"

	"github.com/gorilla/mux"
)

func NewUserController(router *mux.Router, service Services.IUserService) *UserController {
	controller := &UserController{
		userService: service,
	}
	subRoute := router.PathPrefix("/api/User").Subrouter()
	// subRoute.Use(Middlewares.JwtAuthMiddleware)
	subRoute.HandleFunc("/Register", controller.Register).Methods("POST")
	subRoute.HandleFunc("/Login", controller.Login).Methods("POST")
	subRoute.HandleFunc("/GetToken", controller.GetToken).Methods("POST")

	return controller
}

type IUserController interface {
	Register(http.ResponseWriter, *http.Request)
	GetToken(http.ResponseWriter, *http.Request)
	Login(http.ResponseWriter, *http.Request)
}

type UserController struct {
	userService Services.IUserService
}

func (this *UserController) Register(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var body Models.RegisterRequestModel

	json.NewDecoder(req.Body).Decode(&body)

	result := this.userService.Register(body)

	json.NewEncoder(w).Encode(result)
}

func (this *UserController) GetToken(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var body Models.LoginRequestModel

	json.NewDecoder(req.Body).Decode(&body)

	result := this.userService.GetToken(body)

	json.NewEncoder(w).Encode(result)
}

func (this *UserController) Login(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var body Models.LoginRequestModel

	json.NewDecoder(req.Body).Decode(&body)

	result := this.userService.Login(body)

	json.NewEncoder(w).Encode(result)
}
