package Repositories

import (
	"fmt"
	"golang_rest_api/Src/Entities"
	"golang_rest_api/Src/Models"

	"gorm.io/gorm"
)

func NewUserRepository(db *gorm.DB) IUserRepository {
	repo := &UserRepository{
		DB: db,
	}

	return repo
}

type IUserRepository interface {
	GetAll() []Entities.UserEntity
	GetById(id string) (Entities.UserEntity, bool)
	GetByEmail(email string) (Entities.UserEntity, bool)
	Create(param Entities.UserEntity) Models.RepoResultModel
	Update(param Entities.UserEntity) Models.RepoResultModel
	Delete(id string) Models.RepoResultModel
}

type UserRepository struct {
	DB *gorm.DB
}

func (this *UserRepository) Create(param Entities.UserEntity) Models.RepoResultModel {

	result := Models.RepoResultModel{
		IsSuccess: true,
	}

	insertRes := this.DB.Table("golang_mux.user").Create(&param)

	if insertRes.Error != nil {
		result.IsSuccess = false
		result.Message = "insert datafailed : " + fmt.Sprint(insertRes.Error)
	} else {
		result.Value = param
	}

	return result
}

func (this *UserRepository) Update(param Entities.UserEntity) Models.RepoResultModel {
	result := Models.RepoResultModel{
		IsSuccess: true,
	}

	var dataOld []Entities.UserEntity
	this.DB.Table("golang_mux.user").Select("*").Where("id = ?", param.Id).Scan(&dataOld)

	updateErr := this.DB.Table("golang_mux.user").Updates(param).Where("id = ?", param.Id).Error

	if updateErr != nil {
		result.IsSuccess = false
		result.Message = "update failed :" + fmt.Sprint(updateErr)
	} else {
		result.Value = param
	}

	return result
}

func (this *UserRepository) Delete(Id string) Models.RepoResultModel {
	result := Models.RepoResultModel{
		IsSuccess: true,
	}

	var dataOld []Entities.UserEntity
	this.DB.Table("golang_mux.user").Select("*").Where("id = ?", Id).Scan(&dataOld)

	updateErr := this.DB.Table("golang_mux.user").Delete(&dataOld).Error
	if updateErr != nil {
		result.IsSuccess = false
		result.Message = "update failed :" + fmt.Sprint(updateErr)
	} else {
		result.Value = dataOld
	}

	return result
}

func (this *UserRepository) GetAll() []Entities.UserEntity {

	var rows []Entities.UserEntity

	this.DB.Table("golang_mux.user").Select("*").Scan(&rows)

	return rows
}

func (this *UserRepository) GetById(id string) (Entities.UserEntity, bool) {

	var data []Entities.UserEntity
	var isnull = false
	var res Entities.UserEntity
	this.DB.Table("golang_mux.user").Select("*").Where("id = ?", id).Scan(&data)

	if len(data) > 0 {
		res = data[0]
	} else {
		isnull = true
	}

	return res, isnull
}

func (this *UserRepository) GetByEmail(email string) (Entities.UserEntity, bool) {

	var data []Entities.UserEntity
	var isnull = false
	var res Entities.UserEntity
	this.DB.Table("golang_mux.user").Select("*").Where("email = ?", email).Scan(&data)

	if len(data) > 0 {
		res = data[0]
	} else {
		isnull = true
	}

	return res, isnull
}
