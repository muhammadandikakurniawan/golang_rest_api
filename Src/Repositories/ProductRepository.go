package Repositories

import (
	"fmt"
	"golang_rest_api/Src/Entities"
	"golang_rest_api/Src/Models"

	"gorm.io/gorm"
)

func NewProductRepository(db *gorm.DB) IProductRepository {
	repo := &ProductRepository{
		DB: db,
	}

	return repo
}

type IProductRepository interface {
	GetAll() []Entities.ProductEntity
	Create(param Entities.ProductEntity) Models.RepoResultModel
	Update(param Entities.ProductEntity) Models.RepoResultModel
	Delete(id int) Models.RepoResultModel
	GetById(id int) (Entities.ProductEntity, bool)
}

type ProductRepository struct {
	DB *gorm.DB
}

func (this *ProductRepository) Create(param Entities.ProductEntity) Models.RepoResultModel {

	result := Models.RepoResultModel{
		IsSuccess: true,
	}

	insertRes := this.DB.Table("golang_mux.tb_product").Create(&param)

	if insertRes.Error != nil {
		result.IsSuccess = false
		result.Message = "insert datafailed : " + fmt.Sprint(insertRes.Error)
	} else {
		result.Value = param
	}

	return result
}

func (this *ProductRepository) Update(param Entities.ProductEntity) Models.RepoResultModel {
	result := Models.RepoResultModel{
		IsSuccess: true,
	}

	var dataOld []Entities.ProductEntity
	this.DB.Table("golang_mux.tb_product").Select("*").Where("id = ?", param.Id).Scan(&dataOld)

	updateErr := this.DB.Table("golang_mux.tb_product").Updates(param).Where("id = ?", param.Id).Error

	if updateErr != nil {
		result.IsSuccess = false
		result.Message = "update failed :" + fmt.Sprint(updateErr)
	} else {
		result.Value = param
	}

	return result
}

func (this *ProductRepository) Delete(Id int) Models.RepoResultModel {
	result := Models.RepoResultModel{
		IsSuccess: true,
	}

	var dataOld []Entities.ProductEntity
	this.DB.Table("golang_mux.tb_product").Select("*").Where("id = ?", Id).Scan(&dataOld)

	updateErr := this.DB.Table("golang_mux.tb_product").Delete(&dataOld).Error
	if updateErr != nil {
		result.IsSuccess = false
		result.Message = "update failed :" + fmt.Sprint(updateErr)
	} else {
		result.Value = dataOld
	}

	return result
}

func (this *ProductRepository) GetAll() []Entities.ProductEntity {

	var rows []Entities.ProductEntity

	//cara 1
	this.DB.Table("golang_mux.tb_product").Select("*").Scan(&rows)
	//cara 2
	// this.DB.Raw("SELECT * FROM training.tb_product").Scan(&results)

	return rows
}

func (this *ProductRepository) GetById(id int) (Entities.ProductEntity, bool) {

	var data []Entities.ProductEntity
	var isnull = false
	var res Entities.ProductEntity
	this.DB.Table("golang_mux.tb_product").Select("*").Where("id = ?", id).Scan(&data)

	if len(data) > 0 {
		res = data[0]
	} else {
		isnull = true
	}

	return res, isnull
}
