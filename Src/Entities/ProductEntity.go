package Entities

type ProductEntity struct {
	// gorm.Model
	Id      int    `json:"id"`
	Name    string `json:"name"`
	Price   string `json:"price"`
	SkuCode string `json:"sku_code"`
}
