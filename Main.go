package main

// import(
// 	"encoding/json"
// 	"log"
// 	"net/http"
// 	"math/rand"
// 	"strconv"
// )

import (
	"golang_rest_api/Src/Controllers"
	"golang_rest_api/Src/DbContext"
	"golang_rest_api/Src/Repositories"
	"golang_rest_api/Src/Services"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	//init router

	router := mux.NewRouter()

	postGresSqlDbConnection := DbContext.GetConnection()

	//================================================= Repository ========================================================
	var (
		userRepository    = Repositories.NewUserRepository(postGresSqlDbConnection)
		productRepository = Repositories.NewProductRepository(postGresSqlDbConnection)
	)
	//================================================= END Repository ====================================================

	//================================================= Service ===========================================================
	var (
		userService    = Services.NewUserService(userRepository)
		productService = Services.NewProductService(productRepository)
	)
	//================================================= END Service =======================================================

	Controllers.NewUserController(router, userService)
	Controllers.NewProductController(router, productService)

	log.Fatal(http.ListenAndServe(":8080", router))
}
